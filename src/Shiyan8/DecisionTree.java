package Shiyan8;

import java.util.Scanner;

public class DecisionTree {
    private LinkedBinaryTree<String> tree;
    public DecisionTree(){
        String e1 = "你喜欢听民谣吗？";
        String e2 = "那你平时了解过关于民谣歌手的故事吗？";
        String e3 = "那你有没有听过性空山吗？";
        String e4 = "对于民谣歌手你的了解多吗？";
        String e5 = "那你看过中国好歌曲吗？";
        String e6 = "那你知道当代民谣女歌手有哪些吗？";
        String e7 = "你知道第一季的时候有一个惊艳全场的女歌手吗？";
        String e8 = "好的，你可能真不知道。";
        String e9 = "祝星和陈粒的故事你听过吗？";
        String e10 = "你有没有听人说过一个很有自己特色的女民谣歌手？";
        String e11 = "那你应该猜到她是谁了，谢谢！";
        String e12 = "那你应该猜到她是谁了，谢谢！";
        String e13 = "那你应该知道她是谁了，谢谢！";
        String e14 = "那你应该猜到她是谁了，谢谢！";
        String e15 = "好的，你会知道我说的是谁了，谢谢！";
        String e16 = "那你应该猜到她是谁了，谢谢！";
        String e17 = "那你应该猜到她是谁了，谢谢！";
        String e18 = "好的，你可能真不知道。";
        String e19 = "那你应该猜到她是谁了，谢谢！";
        LinkedBinaryTree<String>n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19;
        n16 = new LinkedBinaryTree<String>(e16);
        n17 = new LinkedBinaryTree<String>(e17);
        n9 = new LinkedBinaryTree<String>(e9,n16,n17);
        n18 = new LinkedBinaryTree<String>(e18);
        n19 = new LinkedBinaryTree<String>(e19);
        n10 = new LinkedBinaryTree<String>(e10,n18,n19);
        n8 = new LinkedBinaryTree<String>(e8);
        n4 = new LinkedBinaryTree<String>(e4,n8,n9);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5,n10,n11);
        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e5,n12,n13);
        n14 = new LinkedBinaryTree<String>(e14);
        n15 = new LinkedBinaryTree<String>(e15);
        n7 = new LinkedBinaryTree<String>(e6,n14,n15);
        n2 = new LinkedBinaryTree<String>(e2,n4,n5);
        n3 = new LinkedBinaryTree<String>(e3,n6,n7);
        tree = new LinkedBinaryTree<String>(e1,n2,n3);
    }
    public void diagose() throws Exception {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String>current = tree;

        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equalsIgnoreCase("N"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println (current.getRootElement());
        System.out.println("她的名字叫陈粒");
        System.out.println("她是我最喜欢的歌手");
    }
}
