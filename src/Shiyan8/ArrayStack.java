package Shiyan8;

public class ArrayStack<T> implements Sta<T> {
    private final int LENGTH=10;
    private int count;
    private T[] stack;
    T t3,t4;
    public ArrayStack(T t1, T t2) {
        count =0;
        stack=(T[]) (new Object[LENGTH]);
        push(t1);
        push(t2);
    }
    @Override
    public void push(T element) {
        if(count==stack.length){
            expandCapacity();
        }
        stack[count]=element;
        count++;
    }

    @Override
    public T pop() {
        return null;
    }

    private void expandCapacity(){
        T[] larger =(T[])(new Object[LENGTH*2]);
        for(int index=0;index<stack.length;index++)
            larger[index]=stack[index];
        stack=larger;
    }

    @Override
    public T peek() throws EmptyCollectionException {
        try {
            if(isEmpty())
                throw new EmptyCollectionException("nothing in stack");
        }
        catch (EmptyCollectionException exception)
        {
            System.out.println("nothing in stack");
        }
        return stack[count-1];
    }

    @Override
    public boolean isEmpty() {
        if(stack.length==0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return stack.length;
    }

    @Override
    public String toString() {
        String result="<top>"+"\n";
        for(int i=count-1;i>=0;i--){
            result+=stack[i]+"\n";
        }
        return result;
    }
}