package Shiyan8;

public class LinearNode<T> {
    protected LinearNode<T> next;
    protected T element;
    public LinearNode(){
        next=null;
        element=null;
    }
    public LinearNode(T elem){
        next=null;
        element=elem;
    }

    public LinearNode<T> getNext() {
        return next;
    }

    public void setNext(LinearNode<T> node) {
        next = node;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T ele) {
        element = ele;
    }
}