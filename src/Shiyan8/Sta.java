package Shiyan8;



public interface Sta<T> {
    public void push(T element);
    public T pop() throws EmptyCollectionException;
    public T peek() throws EmptyCollectionException, EmptyCollectionException;
    public boolean isEmpty();
    public int size() throws EmptyCollectionException;
    public String toString();
}