package Shiyan8;

public class LinkedQueue<T> implements Queue<T> {
    private int count;
    private LinearNode<T> front,rear;
    public LinkedQueue(){
        count=0;
        front=rear=null;
    }

    @Override
    public void enqueue(T element) {
        LinearNode<T> node=new LinearNode<>(element);
        if(count==0)
            front=node;
        else
            rear.setNext(node);
        rear=node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException {
        try {
            if (isEmpty())
                throw new EmptyCollectionException("nothing in stack");
        } catch (EmptyCollectionException exception) {
            System.out.println("nothing in stack");
            return null;
        }
        count--;
        LinearNode<T> node = new LinearNode<T>(front.getElement());
        front = front.next;
        return node.getElement();
    }

    @Override
    public T first() throws EmptyCollectionException {
        try {
            if(isEmpty())
                throw new EmptyCollectionException("nothing in stack");
        }
        catch (EmptyCollectionException exception)
        {
            System.out.println("nothing in stack");
        }
        return front.getElement();
    }

    @Override
    public boolean isEmpty() {
        if(count==0)
            return true;
        else
            return false;
    }

    @Override
    public int size() {
        return count;
    }
    public String toString() {
        String result = "";
        LinearNode<T> temp = front;
        while (temp != null) {
            result += temp;
            if(temp.next!=null) {
                temp = temp.next;
            }
        }
        return result;
    }
}