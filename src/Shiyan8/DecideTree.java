package Shiyan8;

public class DecideTree {
    protected BTNode root;
    protected String s;
    public DecideTree(){
        root=new BTNode("你喜欢黄家驹吗？");
        root.left=new BTNode("他的歌很好听吗？");
        BTNode l1=root.left;
        root.right=new BTNode("不，非常喜欢。");
        l1.left=new BTNode("他长得很帅吗？");
        l1.right=new BTNode("不，非常好听");
        BTNode l2=l1.left;
        l2.left=new BTNode("I Want Rock");
        l2.right=new BTNode("你必须喜欢，歌必须好听");
    }
    public void run(){
        System.out.println(root.element);
    }
    public int run(String a) {
        String t = a;
        switch (t) {
            case "y": {
                root = root.left;
                break;
            }
            case "n": {
                root = root.right;
                break;
            }
            default:
                System.out.println("illegal input");
        }
        if (root != null) {
            System.out.println(root.element);
            return 1;
        }
        else
            return -1;
    }
}