public class Book
{
   private String bookname;
   private String auther;
   private String press;
   private String copyrightdate;
   
   
   public Book()
   {
      String bookname = "Null";
      String auther = "Null";
      String press = "Null";
      String copyrightdate = "Null";
   }
   public void setBookname(String name)
   {
      bookname = name;
   }
   public String getBookname()
   {
       return  bookname;      
   }
   public void setAuther(String auth)
   {
       auther = auth;
   }
   public String getAuther()
   {
       return auther;
   } 
   public void setPress(String chubanshe)
   {
       press = chubanshe;
   } 
   public String getPress()
   {
       return press;
   }
   public void setCopyrightdate(String date)
   {
       copyrightdate = date;
   }
   public String getCopyrightdate()
   {
       return copyrightdate;
   }
   public String toString()
   {
    return ("Book : " +bookname + "\nAuther : " + auther + "\nPress : " + press + "\nCopyRightDate : " + copyrightdate);
   }

}

