import java.util.Scanner;

public class  MyOp
{
  public static double calculator (double num1,double num2,String op1)
  {
   
       Scanner input = new Scanner (System.in);
       double result;
       switch (op1)
       {
        case "+":
           result = num1+num2;
           break;
        case "-":
           result = num1-num2;
           break;
        case "*":
           result = num1*num2;
           break;
        case "/":
           result = num1/num2;
           break;
        case "%":
           result = num1%num2;
           break;
        default:
           result=-1;
           break;
        }
       return result;
     
  }

}
