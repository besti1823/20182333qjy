package Shiyan4;

public class JieKouYOU {
    private int numerator, denominator;

    public JieKouYOU(int numer, int denom) {
        if (denom == 0)
            denom = 1;
        else if (denom < 0) {
            denom = -1 * denom;
            numer = -1 * numer;
        }

        numerator = numer;
        denominator = denom;
        reduce();
    }

    public int getNumerator() {
        return numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public JieKouYOU reciprocal() {
        return new JieKouYOU(denominator, numerator);
    }

    public JieKouYOU add(JieKouYOU op2) {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;
        return new JieKouYOU(sum, commonDenominator);
    }

    public JieKouYOU sub(JieKouYOU op2) {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int different = numerator1 - numerator2;
        return new JieKouYOU(different, commonDenominator);

    }

    public JieKouYOU muti(JieKouYOU op2) {
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();
        return new JieKouYOU(numer, denom);
    }

    public JieKouYOU div(JieKouYOU op2) {
        return muti(reciprocal());
    }

    public boolean isLike(JieKouYOU op2) {
        return (numerator == op2.getNumerator() && denominator == op2.getDenominator());
    }

    public String toString() {
        String result;
        if (numerator == 0)
            result = "0";
        else if (denominator == 1)
            result = numerator + "";
        else
            result = numerator + "/" + denominator;
        return result;
    }

    private void reduce() {
        if (numerator != 0) {
            int common = gcd(Math.abs(numerator), denominator);

            numerator = numerator / common;
            denominator = denominator / common;

        }

    }

    private int gcd(int num1, int num2) {
        while (num1 != num2)
            if (num1 > num2)
                num1 = num1 - num2;
            else
                num2 = num2 - num1;
        return num1;
    }

}