package Shiyan4;

import java.util.Scanner;

/**
 * @author sunkun
 * 为了测试分数四则运算
 */
public class YouLiShuTest {
    public static void main(String[] args) {
        // TODO Auto-generated method stub

        // 用户输入两分数和运算符
        Scanner input = new Scanner(System.in);
        System.out.println("请用户输入第一个分数(格式a/b)");
        String data1 = input.next();
        System.out.println("请用户输入要进行运算的运算符(+-*/)");
        String operation = input.next();
        System.out.println("请用户输入第二个分数(格式c/d)");
        String data2 = input.next();

        // 根据用户输入进行具体运算
        YouLiShu cal = new YouLiShu();
        System.out.println("运算结果为:");
        cal.compute(data1, operation, data2);
//	  }
    }
}