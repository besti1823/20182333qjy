package Shiyan4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.lang.*;

/**
 * Created by besti on 2019/9/29.
 */
public class ServerYouLiShu {
    public static void main(String[] args) throws IOException {
        //1.建立一个服务器Socket(ServerSocket)绑定指定端口
        ServerSocket serverSocket = new ServerSocket(2333);
        //2.使用accept()方法阻止等待监听，获得新连接
        Socket socket = serverSocket.accept();
        //3.获得输入流
        InputStream inputStream = socket.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        //获得输出流
        OutputStream outputStream = socket.getOutputStream();
        PrintWriter printWriter = new PrintWriter(outputStream);
        //4.读取用户输入信息
        String info = null;
        System.out.println("服务器已经建立......");
        String n = bufferedReader.readLine();
        if (n != null) {
            System.out.println("我是服务器，用户信息为：" + n);
        }

        char[] arr = n.toCharArray();
        int a = (int) arr[0] - 48;
        int b = (int) arr[2] - 48;
        int c = (int) arr[4] - 48;
        int d = (int) arr[6] - 48;

        String op;
        op = String.valueOf(arr[3]);
        JieKouYOU r1 = new JieKouYOU(a, b);
        JieKouYOU r2 = new JieKouYOU(c, d);
        JieKouYOU r4;
        JieKouYOU r5;
        JieKouYOU r6;
        JieKouYOU r7;
        //给客户一个响应
        switch (op) {
            case "+":
                r4 = r1.add(r2);
                System.out.println(r4);
                String reply1 = "r1 +r2:" + r4;
                printWriter.write(reply1);
                printWriter.flush();
                break;
            case "-":
                r5 = r1.sub(r2);
                String reply2 = "r1 -r2 :" + r5;
                printWriter.write(reply2);
                printWriter.flush();
                break;
            case "*":
                r6 = r1.muti(r2);
                String reply3 = "r1 *r2 :" + r6;
                printWriter.write(reply3);
                printWriter.flush();
                break;
            case "/":
                r7 = r1.div(r2);
                String reply4 = "r1 /r2 :" + r7;
                printWriter.write(reply4);
                printWriter.flush();
                break;
        }
        //5.关闭资源
        //5.关闭资源
        printWriter.close();
        outputStream.close();
        bufferedReader.close();
        inputStream.close();
        socket.close();
        serverSocket.close();
    }
}

