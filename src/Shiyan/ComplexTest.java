package Shiyan;


import junit.framework.TestCase;
import org.junit.Test;


public class ComplexTest extends TestCase {
    Complex c1 = new Complex(2, 2);
    Complex c2 = new Complex(1, 1);

    @Test
    public void testAdd() {
        assertEquals("3.0+3.0i", c1.ComplexAdd(c2).toString());
        System.out.println(c1 + "+" + c2 + "=" + c1.ComplexAdd(c2));
    }

    @Test
    public void testSub() {
        assertEquals("1.0+1.0i", c1.ComplexSub(c2).toString());
        System.out.printf(c1 + "-" + c2 + "=" + c1.ComplexSub(c2));
    }

    @Test
    public void testMulti() {
        assertEquals("0.0+4.0i", c1.ComplexMulti(c2).toString());
        System.out.printf(c1 + "*" + c2 + "=" + c1.ComplexMulti(c2));
    }

    @Test
    public void testDiv() {
        assertEquals("2.0+2.0i", c1.ComplexDiv(c2).toString());
        System.out.printf(c1 + "/" + c2 + "=" + c1.ComplexDiv(c2));
    }

    @Test
    public void testgetRealPart() {
        assertEquals(2.0, Complex.getRealPart(2));
    }

    @Test
    public void testgetImagePart() {
        assertEquals(1.0, Complex.getImagePart(1));
    }

}
