package Shiyan;

import junit.framework.TestCase;

import org.junit.jupiter.api.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    StringBuffer c = new StringBuffer("StringBufferStringBufferStringBuffer");

    @Test
    public void testcharAt() {
        assertEquals('S', a.charAt(0));
        assertEquals('B', a.charAt(6));
        assertEquals('e', a.charAt(10));
    }

    @Test
    public void testcapacity() {
        assertEquals(28, a.capacity());
        assertEquals(40, b.capacity());
        assertEquals(52, c.capacity());
    }

    @Test
    public void testlength() {
        assertEquals(12, a.length());
        assertEquals(24, b.length());
        assertEquals(36, c.length());
    }

    @Test
    public void testindexOf() {
        assertEquals(0, a.indexOf("Str"));
        assertEquals(5, a.indexOf("gBu"));
    }

}