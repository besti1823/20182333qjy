package Shiyan;

public class Complex {

    double R;
    double I;

    public Complex(double R, double I) {
        this.R = R;
        this.I = I;
    }

    public static double getRealPart(double R) {
        return R;
    }

    public static double getImagePart(double I) {
        return I;
    }

    public Complex ComplexAdd(Complex a) {
        return new Complex(R + a.R, I + a.I);
    }

    public Complex ComplexSub(Complex a) {
        return new Complex(R - a.R, I - a.I);
    }

    public Complex ComplexMulti(Complex a) {

        return new Complex(R * a.R - I * a.I, R * a.I + I * a.R);

    }

    public Complex ComplexDiv(Complex a) {

        return new Complex((R * a.I + I * a.R) / (a.I * a.I + a.R * a.R), (I * a.I + R * a.R) / (a.I * a.I + a.R * a.R));

    }

    public String toString() {
        String s = "";
        if (I > 0) {
            s = R + "+" + I + "i";
        }
        if (I == 0) {
            s = R + "";
        }
        if (I < 0) {
            s = R + I + "i";
        }
        return s;
    }
}