package Shiyan6;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

//使用冒泡排序法根据数值大小对链表进行排序。

public class exp3{


    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入你的学号（依次取出的两位数）、日期和时间\n" +
                "例如你的学号是 20172301\n" +
                "\t今天时间是 2018/10/1， 16：23：49秒\n" +
                "数字就是\n" +
                "\t20， 17，23，1， 20， 18，10，1，16，23，49");
        String input =scan.nextLine();
        int[] number = new int[100];
        String[] numbers = input.split(" ");
        for(int a = 0;a<numbers.length;a++)
        {
            number[a] = Integer.parseInt(numbers[a]);
        }
        FirstLinkedlist Head = new FirstLinkedlist(number[0]);
        int nQianjiayu = 1;
        for(int a=1; a < numbers.length;a++)
        {
            FirstLinkedlist node = new FirstLinkedlist(number[a]);
            InsertNode(Head,node);
            nQianjiayu++;
        }
        //打印链表元素。
        System.out.println("链表元素是:");
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("元素总数为:" + nQianjiayu);

        //磁盘读取一个文件， 这个文件有两个数字
        File file = new File("F:\\IdeaProjects\\20182333qjy\\src\\Shiyan6", "num.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine());
        int  num = stringTokenizer.countTokens();
        int[] counts = new int[num];
        int a=0;
        while(stringTokenizer.hasMoreTokens()){
            counts[a]=Integer.parseInt(stringTokenizer.nextToken());
            a++;
        }
        int firstnum =counts[0];//从文件中读取的第一个数
        int secondnum = counts[1];//从文件中读取的第二个数

        /*	从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数。 保留这个链表，继续下面的操作。*/
        FirstLinkedlist node1 = new FirstLinkedlist(firstnum);
        FirstLinkedlist temp = Head;
        for(int b = 0;b<5;b++)
        {
            temp=temp.next;
        }
        InsertNode(Head,temp,node1);
        nQianjiayu++;
        System.out.println("插入从文件读出第一个元素后的链表为：" );
        PrintLinkedlist(Head);
        System.out.println("链表的个数变为："+nQianjiayu);
        //从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。 保留这个链表，并继续下面的操作。
        FirstLinkedlist node2 = new FirstLinkedlist(secondnum);
        InsertNode1(Head,node2);
        nQianjiayu++;
        System.out.println("插入从文件读出第二个元素后的链表为：");
        PrintLinkedlist(node2);
        System.out.println("链表的个数变为："+nQianjiayu);
        //从链表中删除刚才的数字1.  并打印所有数字和元素的总数。
        FirstLinkedlist node3 = Head;
        while(node3.number!=2018)
        {
            node3=node3.next;
        }
        Deletenode(Head,node3);
        nQianjiayu--;
        System.out.println("删除插入的第一个元素后链表为：");
        PrintLinkedlist(node2);
        System.out.println("链表的个数变为："+nQianjiayu);
        //使用冒泡排序法根据数值大小对链表进行排序。
        System.out.println("用冒泡排序后的链表：");
        Sort(node2, nQianjiayu);

    }
    //链表的输出方法。
    public static void PrintLinkedlist(FirstLinkedlist Head) {
        FirstLinkedlist node = Head;//链表的头不能动。
        while (node != null) {
            System.out.print(  node.number );
            node = node.next;
            if(node!=null)
                System.out.print(",");
            else
                System.out.print(" ");

        }
    }
    //尾插法的方法。
    public static void InsertNode(FirstLinkedlist Head, FirstLinkedlist node) {
        FirstLinkedlist temp = Head;
        //遍历链表，找到链表末尾。
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }


    //头插法的方法。
    public static void InsertNode1(FirstLinkedlist Head, FirstLinkedlist node) {
        node.next = Head;
        Head = node;
        System.out.print(Head .number+" ");
    }
    //链表的中间插入。
    public static void InsertNode(FirstLinkedlist Head, FirstLinkedlist node1, FirstLinkedlist node2) {
        FirstLinkedlist point = Head;
        while ((point.number != node1.number) & point != null) {
            point = point.next;
        }
        if (point.number == node1.number) {
            node2.next = point.next;
            point.next = node2;
        }
    }
    //链表的删除方法。
    public static void Deletenode(FirstLinkedlist Head, FirstLinkedlist node) {//这里的node要指向需要删除对象的前一个节点。
        FirstLinkedlist prenode = Head, currentnode = Head;
        while (prenode != null) {
            if (currentnode != node)
            {
                prenode = currentnode;
                currentnode = currentnode.next;
            }
            else {
                break;
            }
        }
        prenode.next = currentnode.next;

    }
    //链表的冒泡排序法。
    public static void Sort(FirstLinkedlist Head, int nQianjiayu){
        FirstLinkedlist temp = Head;
        FirstLinkedlist a,b;
        a = temp;
        b = a.getNext();
        for(int i = 0;i<nQianjiayu-1;i++)
        {
            a = temp;
            b = a.getNext();
           for(int j =0;j<nQianjiayu-i-1;j++)
           {
               if(a.getElement()<b.getElement())
               {
                  int t;
                  t = a.getElement();
                  a.setElement(b.getElement());
                  b.setElement(t);
               }
               a=a.getNext();
               b=b.getNext();
           }
            PrintLinkedlist(Head);
            System.out.println();
        }

    }

}
