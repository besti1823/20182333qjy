package KeHouTi.week8;

public class SortTest {
    public static void main(String[] args) {

        String list[] = {"我","是","钱佳禹","2018","2333"};

        Integer list1[] = {12,23,34,45,56,67,78,89};

        Double list2[] = {2.34,4.2,1.2,2.0,8.1,6.4};

        System.out.println("在同一列表上执行这些排序算法（list2）");
        System.out.println("使用选择排序：");
        Sort.selectionSort(list2);
        System.out.println();
        System.out.println("使用插入排序：");
        Sort.insertionSort(list2);
        System.out.println();
        System.out.println("使用冒泡排序：");
        Sort.bubbleSort(list2);
        System.out.println();
        System.out.println("使用快速排序：");
        Sort.quickSort(list2);
        System.out.println();
        System.out.println("使用归并排序：");
        Sort.mergeSort(list2);
        System.out.println();

        for(double number:list2)
            System.out.print(number+" ");

        System.out.println();
        System.out.println("尝试使用不同的列表，且至少包括一个已经排好序的列表");
        System.out.println("使用快速排序对一个字符串列表进行排序：");
        Sort.quickSort(list);
        for(String line:list)
            System.out.print(line+"\t");
        System.out.println();

        System.out.println("使用归并排序对一个整数型列表进行排序：");
        Sort.mergeSort(list1);
        for(int number:list1)
            System.out.print(number+"\t");
    }
}
