package KeHouTi.week7;

public class LinkedQueueTest {
    public static void main(String[] args) {
        LinkedQueue<Integer> queue=new LinkedQueue<>();
        queue.enqueue(2);
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(8);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(3);
        queue.enqueue(3);
        System.out.println("输出队列:"+queue);
        System.out.println("输出队首:"+queue.first());
        System.out.println("队列长度:"+queue.size());
        System.out.println("队列是否为空:"+queue.isEmpty());
        System.out.println("队列删除:"+queue.dequeue());
        System.out.println("输出队尾:"+queue);
    }
}
