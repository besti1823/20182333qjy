package KeHouTi.week7;

import java.util.Stack;

public class ArrayStack<T> extends Stack<T> {

    private final int DEFAULT_CAPACITY=10;
    private int count;
    private T[] stack;

    public ArrayStack() {
        count =0;
        stack=(T[]) (new Override[DEFAULT_CAPACITY]);
    }

    @Override
    public T push(T element) {
        if(count ==stack.length);
        expandCapacity();
        stack[count]=element;
        count++;
        return element;
    }

    private void expandCapacity() {
        T[] larger=(T[])(new Object[stack.length*2]);
        for(int index=0;index<stack.length;index++)
            larger[index]=stack[index];
        stack=larger;
    }

    @Override
    public T pop()  {

        T result=stack[count-1];
        count--;
        return result;
    }

    @Override
    public T peek() {

        T result=stack[count-1];
        return result;
    }

    @Override
    public boolean isEmpty() {
        if(count==0)return true;
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result="<top of stack>";
        for(int index=count-1;index>=0;index--)
            result +=stack[index]+"\n";
        return result+ "<bottom of stack>";
    }
}
