package KeHouTi.week7;

import java.util.Stack;
import java.util.StringTokenizer;

public class ZhuanHuan {
    private Stack<String> s1=new Stack<>();
    private Stack<String> s2=new Stack<>();
    private String input;
    private String result;

    public ZhuanHuan(String input) {
        this.input = input;
    }

    public String doString() {
        StringTokenizer stn = new StringTokenizer(input, " ");

        while (stn.hasMoreElements()) {
            String temp = stn.nextToken();
            s1.push(temp);
        }

        while (!s1.empty()){
            String temp1=s1.pop();
            char temp2=temp1.charAt(0);
            //是运算符
            if ((temp2<'0'||temp2>'9')&&temp1.length()==1){
                String num2=s2.pop();
                String num1=s2.pop();
                result=num1+num2+temp2;
                s2.push(result);
            }
            //是数字
            else {
                s2.push(temp1);
            }
        }
        return result;
    }
}
