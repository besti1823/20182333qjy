package KeHouTi;

public class Graph {
    private double v = 0;
    private double s = 0;
    private double l = 0;
    private double h = 0;
    private double w = 0;

    public double getV() {
        return v;
    }

    public void setV(double v) {
        this.v = v;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }

    public double getL() {
        return l;
    }

    public void setL(double l) {
        this.l = l;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public void CalcuV() {
        v = h * w * l;
    }

    public void CalcuS() {
        s = 2 * h * w + 2 * h * l + 2 * w * l;
    }

    @Override
    public String toString() {
        return "Graph{" +
                "v=" + v +
                ", s=" + s +
                ", l=" + l +
                ", h=" + h +
                ", w=" + w +
                '}';
    }
}