package KeHouTi.week10;

import java.util.Iterator;
//数据输入
public class GraphTest {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addVertex("A");
        graph.addVertex("B");
        graph.addVertex("C");
        graph.addVertex("D");

        graph.addEdge("A","B");
        graph.addEdge("B","C");
        graph.addEdge("A","C");
        graph.addEdge("B","D");
        System.out.println(graph.toString());
        Iterator it = graph.iteratorBFS("C") ;
        String str = " ";
        while (it.hasNext()){

            str += it.next () + "  ";

        }
        System.out.println(str);
        System.out.println(graph.shortestPathLength("B", "B"));
        System.out.println(graph.getMST());
    }
}