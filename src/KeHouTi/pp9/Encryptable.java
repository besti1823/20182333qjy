package KeHouTi.pp9;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface Encryptable {
    public String encrypt() throws UnsupportedEncodingException, IOException, ClassNotFoundException;
    public String decrypt() throws IOException, ClassNotFoundException;
}