package KeHouTi.pp9;
import java.io.IOException;
public class SecretTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Encryptable hush ;

        Secret a=new Secret("Java is difficult!");
        Password b=new Password("I'm very hard!");

        hush=a;
        System.out.println(hush);
        hush.encrypt();
        System.out.println(hush);
        hush.decrypt();
        System.out.println(hush);

        hush=b;
        System.out.println(hush);
        hush.encrypt();
        System.out.println(hush);
        hush.decrypt();
        System.out.println(hush);
    }
}
