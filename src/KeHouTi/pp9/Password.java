package KeHouTi.pp9;

import java.io.*;
import java.math.BigInteger;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
//RAS加解密
public class Password implements Encryptable{
    private String message;
    private boolean encrypted;
    private BigInteger e;
    private BigInteger n;

    public Password(String msg) {
        message = msg;
        encrypted=false;
    }

    @Override
    public String encrypt() throws IOException, ClassNotFoundException {
        if(!encrypted){
            //获取公钥及参数e.n
            FileInputStream f=new FileInputStream("Skey_RSA_pub.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPublicKey pbk=(RSAPublicKey)b.readObject( );
            e=pbk.getPublicExponent();
            n=pbk.getModulus();
            // 明文 m
            byte ptext[]=message.getBytes("UTF8");
            BigInteger m=new BigInteger(ptext);
            // 计算密文c
            BigInteger c=m.modPow(e,n);
            message=c.toString();
            encrypted=true;
        }
        return null;
    }

    @Override
    public String decrypt()throws IOException,ClassNotFoundException {
        if(encrypted){
            BigInteger c=new BigInteger(message);
            //读取私钥
            FileInputStream f=new FileInputStream("Skey_RSA_priv.dat");
            ObjectInputStream b=new ObjectInputStream(f);
            RSAPrivateKey prk=(RSAPrivateKey)b.readObject( );
            BigInteger d=prk.getPrivateExponent();
            //获取私钥参数及解密
            BigInteger n=prk.getModulus();
            BigInteger m=c.modPow(d,n);
            byte[] mt=m.toByteArray();
            message="";
            for(int index=0;index<mt.length;index++){
                message = message + (char)mt[index];
            }
            encrypted=false;
        }
        return message;
    }

    public String toString(){
        return message;
    }
}