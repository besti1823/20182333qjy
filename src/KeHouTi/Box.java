package KeHouTi;

public class Box {
    private double h;
    private double w;
    private double d;
    private boolean full;

    public Box(double height, double width, double depth, boolean full) {
        this.h = height;
        this.w = width;
        this.d = depth;
        this.full = false;
    }

    public double getHeight() {
        return h;
    }

    public void setHeight(double height) {
        this.h = height;
    }

    public double getWidth() {
        return w;
    }

    public void setWidth(double width) {
        this.w = width;
    }

    public double getDepth() {
        return d;
    }

    public void setDepth(double depth) {
        this.d = depth;
    }

    public boolean isFull() {
        return full;
    }

    public void setFull(boolean full) {
        this.full = full;
    }

    @Override
    public String toString() {
        return "Box{" +
                "height=" + h +
                ", width=" + w +
                ", depth=" + d +
                '}';
    }
}