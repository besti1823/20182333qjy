package KeHouTi;

public class BoxTest {
    public static void main(String[] args) {
        Box a = new Box(1.0, 2.0, 3.0, false);
        Box b = new Box(2.0, 3.0, 4.0, false);
        System.out.println("Size of the Box is:");
        System.out.println("Height:2.0");
        System.out.println("Height:3.0");
        System.out.println("Height:4.0");
        String s = a.toString();
        System.out.println(s);
        double v = 24;
        boolean c;
        if (b.getHeight() * b.getDepth() * b.getWidth() == v) {
            c = b.isFull();
            c = true;
        } else
            c = b.isFull();
        if (c)
            System.out.println("The Box is full!");
        else
            System.out.println("The Box is not full!");
    }
}