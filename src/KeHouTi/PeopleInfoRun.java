package KeHouTi;

public class PeopleInfoRun {
    public static void main(String[] args) {
        Age a = new Age();
        Country b = new Country();
        Income c = new Income();

        System.out.println(a.toString());
        a.setAge(14);
        System.out.println(a.getAge());

        System.out.println(b.toString());
        b.setCountry("China");
        System.out.println(b.getCountry());

        System.out.println(c.toString());
        c.setIncome(10000);
        System.out.println(c.getIncome());
    }
}