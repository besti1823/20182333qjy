package KeHouTi;

public class Age extends PeopleInfo {
    private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Age";
    }
}