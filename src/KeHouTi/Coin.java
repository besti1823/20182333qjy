package KeHouTi;

public class Coin {
    enum Face {H, T}

    public Face b = Face.H;
    public Face c = Face.T;
    private int face;
    private boolean a;

    public Coin() {
        flip();
    }

    public void flip() {
        face = (int) (Math.random() * 2);
        if (face == b.ordinal())
            a = true;
        else
            a = false;
    }

    public boolean isHeads() {
        return (a);
    }

    public String toString() {
        return (a) ? "Heads" : "Tails";
    }
}
