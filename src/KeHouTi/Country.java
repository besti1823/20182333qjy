package KeHouTi;

public class Country extends PeopleInfo {
    private String country;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String toString() {
        return "Country";
    }
}