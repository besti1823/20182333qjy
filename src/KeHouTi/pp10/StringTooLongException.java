package KeHouTi.pp10;

public class StringTooLongException extends Exception{
    public StringTooLongException(String s) {
        super(s);
    }
}