package KeHouTi.pp10;

import java.util.Scanner;

public class StringLong{
    public static void main(String[] args) throws StringTooLongException {
        Scanner in=new Scanner(System.in);
        String s = null;
        do{
            System.out.println("请输入一段字符串(输入DONE是停止):");
            s = in.nextLine();
            if(s.length()>=20){
                throw new StringTooLongException("多字符输入过多（>20）！");
            }
        }while(!s.equals("DONE"));
    }
}