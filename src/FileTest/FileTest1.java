package FileTest;

import java.io.*;

public class FileTest1 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("F:\\IdeaProjects\\20182333qjy\\src\\FileTest", "HelloWorld.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        //（2）文件读写
        //第一种：字节流读写，先写后读
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'H', 'e', 'l', 'l', 'o', ',', 'W', 'o', 'r', 'l', 'd', '!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available() > 0) {
            System.out.print((char) inputStream1.read() + "  ");
        }
        inputStream1.close();

    }
}