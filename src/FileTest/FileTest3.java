package FileTest;

import java.io.*;

public class FileTest3 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("HelloDKY.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        //（2）文件读写
        //第二种：字符流读写，先写后读(两种读)
        Writer writer1 = new FileWriter(file);
        writer1.write("");
        writer1.flush();
        writer1.append("Hello,DKY!");
        writer1.flush();
        Reader reader1 = new FileReader(file);

        while (reader1.ready()) {
            System.out.print((char) reader1.read() + "  ");
        }

    }
}


