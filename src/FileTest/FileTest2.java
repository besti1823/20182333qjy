package FileTest;

import java.io.*;

public class FileTest2 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("HelloJava.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        //（2）文件读写
        //============================BufferedInputStream====================================
        byte[] buffer = new byte[1024];
        String content1 = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);
        while ((flag = bufferedInputStream.read(buffer)) != -1) {
            content1 += new String(buffer, 0, flag);
        }
        System.out.println(content1);
        bufferedInputStream.close();
        //====================================BufferedOutputstream================================================
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "Hello,Java!";
        bufferedOutputStream2.write(content2.getBytes(), 0, content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();


    }
}
