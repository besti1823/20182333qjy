package FileTest;

import java.io.*;

public class FileTest4 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("HelloIdea.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        //（2）文件读写
        //第二种：字符流读写，先写后读(两种读)
        Writer writer2 = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(writer2);
        String content = "Hello,Idea!";
        bufferedWriter.write(content, 0, content.length());
        bufferedWriter.flush();
        bufferedWriter.close();
        Reader reader1 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader1);

        while ((content = bufferedReader.readLine()) != null) {
            System.out.println(content);
        }
    }
}
