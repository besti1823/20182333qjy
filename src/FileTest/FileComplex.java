package FileTest;

import java.io.*;
import java.sql.SQLOutput;
import java.util.Scanner;
import java.util.SortedMap;

public class FileComplex {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("F:\\IdeaProjects\\20182333qjy\\src\\FileTest", "FileComplex.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        int R1, R2, I1, I2, R, I;
        Scanner scan = new Scanner(System.in);
        Writer writer1 = new FileWriter(file);
        System.out.println("请输入第一个复数的实部和虚部:");
        R1 = scan.nextInt();
        I1 = scan.nextInt();
        System.out.println("请输入第二个复数的实部和虚部:");
        R2 = scan.nextInt();
        I2 = scan.nextInt();
        R = R1 + R2;
        I = I1 + I2;
        writer1.write("");
        writer1.flush();
        writer1.append(+R1 + "+" + I1 + "i" + "\n");
        writer1.flush();
        writer1.write("");
        writer1.flush();
        writer1.append(+R2 + "+" + I2 + "i" + "\n");
        writer1.flush();

        Reader reader1 = new FileReader(file);
        while (reader1.ready()) {
            System.out.print((char) reader1.read() + "  ");
        }
        System.out.println(+R1 + "+" + I1 + "i" + "+" + R2 + "+" + I2 + "i" + "=");
        System.out.println(+R + "+" + I + "i");
        writer1.write("");
        writer1.flush();
        writer1.append(+R + "+" + I + "i" + "\n");
        writer1.flush();

    }
}

