package TextBook.PP2;

public class pp2_6 {
    public static void main(String[] args) {
        int sides = 7;  // declaration with initialization

        System.out.println("A heptagon has " + sides + " sides.");

        sides = 10;  // assignment statement

        System.out.println("A decagon has " + sides + " sides. ");

        sides = 12;

        System.out.println("A dodecagon has " + sides + " sides.");
    }
}