package 链表实践;

public class Fangfa {
    private intNode head;
    private int total;
    public Fangfa(){
        head = null;
        total = 0;
    }
    //添加操作
    public void add(int num)
    {
        intNode node = new intNode(num);
        intNode temp;
        if (head == null)
        {
            head = node;
        }
        else
        {
            temp = head;
            while (temp.next != null)
            {
                temp = temp.next;
            }
            temp.next = node;
        }
        total++;
    }
    //插入操作
    public void insert(int place, int number)
    {
        intNode node = new intNode(number);
        intNode temp1, temp2;
        if(sum() >= place)
        {
            if(place == 0)
            {
                node.next = head;
                head = node;
            }
            else
            {
                temp1 = head;
                temp2 = head.next;
                for(int a = 1; a < place; a++)
                {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if(temp2 != null)
                {
                    node.next = temp2;
                    temp1.next = node;
                }
                else
                {
                    temp1.next = node;
                }
            }
            total++;
        }
        else{
            System.out.println( + place +"fail");
        }
    }
    //删除操作
    public void delete(int number)
    {
        intNode temp = head;
        if(temp.number == number)
        {
            head = temp.next;
        }
        else {
            while (!(temp.next.getNumber() == number))
            {
                temp = temp.next;
            }
            temp.next = temp.next.next;
        }
        total--;
    }
    public int sum(){
        return total;
    }
    // 排序操作（冒泡排序或者选择排序）  ，，，，，，，
    public void sort()
    {

        intNode temp1 = head, temp2 = null;
        int num = 0;
        while(temp1.next != temp2){
            while(temp1.next != temp2){
                if(temp1.next.getNumber() < temp1.getNumber()){
                    int number = temp1.getNumber();
                    temp1.setNumber(temp1.next.getNumber());
                    temp1.next.setNumber(number);
                }
                temp1 = temp1.next;
                num++;
                intNode temp = head;

                while (temp != null)
                {
                    temp = temp.next;
                }
            }
            temp2 = temp1;
            temp1 = head;
        }
    }
    @Override
    public String toString(){
        String result = "";
        intNode temp = head;
        while(temp != null){
            result += temp.getNumber() + " ";
            temp = temp.next;
        }
        return result;
    }
    public static class intNode {
        public int number;
        public intNode next;

        public intNode(int figure) {
            number = figure;
            next = null;
        }
        public int getNumber() {
            return number;
        }
        public void setNumber(int number) {
            this.number = number;
        }
    }
}

