package Shijian;
import java.util.Scanner;

public class HouZhuiTest {
    public static void main(String[] args) {
        String exp;
        int result;

        Scanner scan = new Scanner(System.in);

        HouZhui evaluator = new HouZhui();
        System.out.println("计算重复输入后缀表达式 : 7  4  -3  *  1  5  +  /  *");
        exp = scan.nextLine();
        System.out.println("后缀表达式为： " + exp);
        result = evaluator.calculate(exp);
        System.out.println("结果为： " + result);
    }
}