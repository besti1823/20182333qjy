package Shijian;

public class Zoo {
    private Animal[] animallist;

    public Zoo() {
        animallist = new Animal[3];
        animallist[0] = new Cat("喵喵", "01");
        animallist[1] = new Dog("汪汪", "02");
        animallist[2] = new Pig("佩奇", "03");
    }

    public void feedall() {
        for (Animal animal : animallist) {
            System.out.println(animal);
            animal.feed();
        }
        /*int count = 0;
        Animal animal1823;
        for(animal1823=(Animal) animallist[0]; count<5; animal1823 = (Animal) animallist[++count]){
            System.out.println(animal1823);
        }*/

    }
}