package Shijian;

import java.util.Arrays;
import java.util.EmptyStackException;

public class LinkedStack<T> implements StackADT<T> {
    private LinearNode<T> top;
    private int count;

    public LinkedStack() {
        count=0;
        top=null;
    }

    public void push(T element){
        LinearNode<T> temp=new LinearNode<T>(element);

        temp.setNext(top);
        top=temp;
        count++;
    }

    public T pop() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("stack");
        }
        T result=top.getElement();
        top=top.getNext();
        count--;

        return result;
    }

    public T peek() throws EmptyCollectionException{
        if(isEmpty()){
            throw new EmptyCollectionException("stack");
        }

        T result=top.getElement();
        return result;
    }

    @Override
    public boolean isEmpty() {
        if(top==null){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        String result="";
        LinearNode<T> temp=top;
        while(temp.getNext()!=null){
            result+=temp.getElement().toString();
            temp=temp.getNext();
        }
        result+=temp.getElement().toString();
        return result;
    }
}