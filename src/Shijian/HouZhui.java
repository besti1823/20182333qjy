package Shijian;

import java.util.Scanner;
import java.util.Stack;

public class HouZhui {
    private Stack<Integer> stack;

    public HouZhui() {
        stack = new Stack<Integer>();
    }

    public int calculate(String expr){
        int num1,num2, r=0;
        String token;
        Scanner parser = new Scanner(expr);

        while (parser.hasNext()){
            token = parser.next();

            if (isOperator(token)){
                num2 = (stack.pop()).intValue();
                num1 = (stack.pop()).intValue();
                r = evaluateSingleOperator(token.charAt(0),num1,num2);
                stack.push(r);
            }else {
                stack.push(Integer.parseInt(token));
            }
        }
        return r;
    }

    private boolean isOperator(String token){
        return (token.equals("+") || token.equals("-") || token.equals("*") || token.equals("/"));
    }

    private int evaluateSingleOperator(char op, int num1 , int num2){
        int result = 0;

        switch (op){
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1-num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
        }

        return result;
    }
}