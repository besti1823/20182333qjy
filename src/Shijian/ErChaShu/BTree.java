package Shijian.ErChaShu;

import java.util.*;
class TreeNode{
    char c;
    TreeNode lchild;
    TreeNode rchild;
    public TreeNode(char c) {
        this.c = c;
    }
}
public class BTree{
    public static int i = 0;
    public static TreeNode CreateTree(String s) {
        TreeNode root = null;
        if (s.charAt(i) != '#') {
            root = new TreeNode(s.charAt(i));
            i++;
            root.lchild = CreateTree(s);
            root.rchild = CreateTree(s);
        } else {
            i++;
        }
        return root;
    }
    public static void levelTravel(TreeNode root){
        if(root==null)return;
        Queue<TreeNode> tree=new LinkedList<TreeNode>();
        tree.add(root);
        System.out.println("层次遍历输出:");
        while(!tree.isEmpty()){
            TreeNode temp =  tree.poll();
            System.out.print(temp.c);
            if(temp.lchild != null)tree.add(temp.lchild);
            if(temp.rchild != null)tree.add(temp.rchild);
        }
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入序列:");
        while(scan.hasNext()){
            String c = scan.nextLine();
            TreeNode root =  CreateTree(c);
            levelTravel(root);
        }

    }

}
