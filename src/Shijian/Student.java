package Shijian;

public class Student implements People {

    @Override
    public String speak() {
        System.out.println("I can learn.");
        return "I can learn.";
    }

    @Override
    public String run() {
        System.out.println("I can run.");
        return "I can run.";
    }

    public static void main(String[] args) {
        Student a = new Student();
        a.speak();
        a.run();
    }
}
