package Shijian;

import java.util.Scanner;

public class CalculatorJieKou {
    public static void main(String[] args) {
        for (; ; ) {
            System.out.println("Please select mode:Rational or Complex or Quit(R/C/Q)");
            Scanner scan = new Scanner(System.in);
            String s = scan.next();
            char ch = s.charAt(0);

            if (ch == 'R' || ch == 'r') {
                System.out.println("Please input 分数num1,den1,分数den2,den2: ");
                int num1 = scan.nextInt();
                int den1 = scan.nextInt();
                int num2 = scan.nextInt();
                int den2 = scan.nextInt();
                RationalNumber r1 = new RationalNumber(num1, den1);
                RationalNumber r2 = new RationalNumber(num2, den2);
                RationalNumber r3;
                RationalNumber r4;
                RationalNumber r5;
                RationalNumber r6;
                RationalNumber r7;
                System.out.println("First rational number: " + r1);
                System.out.println("Second rational number:" + r2);
                if (r1.isLike(r2))
                    System.out.println("r1 and r2 are equal.");
                else
                    System.out.println("r1 and r2 are not equal.");
                r3 = r1.reciprocal();
                System.out.println("The reciprocal of r1 is:" + r3);
                System.out.println("Please input the OP:");
                String op = scan.next();
                switch (op) {
                    case "+":
                        r4 = r1.add(r2);
                        System.out.println("r1 + r2 = " + r4);
                        break;
                    case "-":
                        r5 = r1.subtract(r2);
                        System.out.println("r1 - r2 = " + r5);
                        break;
                    case "*":
                        r6 = r1.multiply(r2);
                        System.out.println("r1 * r2 = " + r6);
                        break;
                    case "/":
                        r7 = r1.divide(r2);
                        System.out.println("r1 / r2 = " + r7);
                        break;
                }
            } else if (ch == 'C' || ch == 'c') {
                System.out.println("Please input 实数R1，I1 实数R1,I2: ");
                int R1 = scan.nextInt();
                int R2 = scan.nextInt();
                int I1 = scan.nextInt();
                int I2 = scan.nextInt();
                complex r1 = new complex(R1, I1);
                complex r2 = new complex(R2, I2);
                Boolean r3;
                complex r4;
                complex r5;
                complex r6;
                complex r7;
                System.out.println("First rational number（r1）: " + r1);
                System.out.println("Second rational number（r2）:" + r2);

                System.out.println("Choose calculation kind: (+ - * / >)");
                String m = scan.next();
                switch (m) {
                    case "+":
                        r4 = r1.add(r2);
                        System.out.println("r1 + r2 = " + r4);
                        break;
                    case "-":
                        r5 = r1.sub(r2);
                        System.out.println("r1 - r2 = " + r5);
                        break;
                    case "*":
                        r6 = r1.mul(r2);
                        System.out.println("r1 * r2 = " + r6);
                        break;
                    case "/":
                        r7 = r1.div(r2);
                        System.out.println("r1 / r2 = " + r7);
                        break;
                    case ">":
                        r3 = r1.Compare(r2);
                        if (r3 == true)
                            System.out.println("r1<r2");
                        else
                            System.out.println("r2>r1");
                        break;
                }
            } else return;
        }
    }
}

