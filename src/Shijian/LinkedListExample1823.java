package Shijian;

public class LinkedListExample1823 {
    Student1  student1 = new Student1("赵沛凝","20182301","吃");
    Student1  student2 = new Student1("赵沛凝","20182301","吃");
    Student1  student3 = new Student1("赵沛凝","20182301","吃");
    Student1 head =  student1;

    public static Student1 InserNode(Student1 Head,Student1 node){
        //尾插法: 在链表的尾部插入节点。
        Student1 temp = Head;
        while (temp.next!= null){
            temp = temp.next;
        }
        temp.next = node;
        return  Head;
    }

}
