package Shijian.BianLi;

import java.util.LinkedList;
import java.util.Scanner;
import java.util.Stack;

class TreeNode{
    String data;
    TreeNode lchild ;
    TreeNode rchild;
    public String getData()
    {
        return data;
    }
    public TreeNode getLchild()	{
        return lchild;
    }
    public TreeNode getRchild()	{
        return rchild;
    }
    public void setNode(String data, TreeNode left2, TreeNode right2){
        this.data = data;
        lchild = left2;
        rchild = right2;
    }
    public TreeNode(){

    }
}