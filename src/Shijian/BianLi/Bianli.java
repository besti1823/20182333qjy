package Shijian.BianLi;

import java.util.LinkedList;
import java.util.Stack;

class Count {
        public static int count = 0;
    }


    public class Bianli {
        public static TreeNode createTree(TreeNode root, String[] a, int i) {

            if (i < a.length) {
                if (a[i] == "#") {
                    root = null;
                } else {
                    TreeNode left = new TreeNode();
                    TreeNode right = new TreeNode();
                    root.setNode(a[i], createTree(left, a, ++Count.count), createTree(right, a, ++Count.count));
                }
            }
            return root;
        }


        public static void traverse(TreeNode root) {
            if (root != null) {
                System.out.print(root.getData() + " ");
                traverse(root.getLchild());
                traverse(root.getRchild());
            } else {
                System.out.print(" ");
            }
        }

        public static void preOrderTraverse(TreeNode root) {
            if (root != null) {
                System.out.print(root.getData());
                if (root.getLchild() != null) {
                    preOrderTraverse(root.getLchild());
                }
                if (root.getRchild() != null) {
                    preOrderTraverse(root.getRchild());
                }
            } else System.out.print(" ");
        }

        public static void zhongxu(TreeNode root) {
            if (root != null) {

                if (root.getLchild() != null) {
                    zhongxu(root.getLchild());
                }
                System.out.print(root.getData() + " ");
                if (root.getRchild() != null) {
                    zhongxu(root.getRchild());
                }
            } else System.out.print(" ");
        }

        public static void houxu(TreeNode root) {
            if (root != null) {

                if (root.getLchild() != null) {
                    zhongxu(root.getLchild());
                }

                if (root.getRchild() != null) {
                    zhongxu(root.getRchild());
                }
                System.out.print(root.getData() + " ");
            } else System.out.print("-");
        }

        public static void levelOrderTraverse(TreeNode root) {
            TreeNode temp = root;
            LinkedList<TreeNode> linkedList = new LinkedList();
            linkedList.add(temp);
            while (!linkedList.isEmpty()) {
                temp = linkedList.poll();
                System.out.print(temp.data + " ");
                if (temp.getLchild() != null) {
                    linkedList.add(temp.getLchild());
                }
                if (temp.getRchild() != null) {
                    linkedList.add(temp.getRchild());
                }
            }
        }

        public static void preOrderStack2(TreeNode root) {
            Stack<TreeNode> stack = new Stack<>();
            TreeNode treeNode = root;
            while (treeNode != null || !stack.isEmpty()) {
                //将左子树点不断的压入栈
                while (treeNode != null) {
                    //先访问再入栈
                    System.out.print(treeNode.data + " ");
                    stack.push(treeNode);
                    treeNode = treeNode.lchild;
                }
                //出栈并处理右子树
                if (!stack.isEmpty()) {
                    treeNode = stack.pop();
                    treeNode = treeNode.rchild;
                }

            }

        }

        public static void inOrderStack(TreeNode treeNode) {
            Stack<TreeNode> stack = new Stack<>();
            while (treeNode != null || !stack.isEmpty()) {
                while (treeNode != null) {
                    stack.push(treeNode);
                    treeNode = treeNode.lchild;
                }
                //左子树进栈完毕
                if (!stack.isEmpty()) {
                    treeNode = stack.pop();
                    System.out.print(treeNode.data + " ");
                    treeNode = treeNode.rchild;
                }
            }
        }


        public static void main(String[] args) {
            String[] a = {"A", "B", "#", "C", "D", "#", "#", "#", "E", "#", "F", "#", "#"};
            TreeNode root = new TreeNode();
//        int ii=0;
            root = createTree(root, a, 0);
            if (root == null) {
                System.out.println("null");
            }
            System.out.println("前序遍历:");
            traverse(root);
            System.out.println();
            //preOrderTraverse(root);

            System.out.println("中序遍历:");
            zhongxu(root);
            System.out.println();

            System.out.println("后序遍历:");
            houxu(root);
            System.out.println();

            System.out.println("层序遍历:");
            levelOrderTraverse(root);
            System.out.println();

            System.out.println("前序非递归:");
            preOrderStack2(root);
            System.out.println();

            System.out.println("中序非递归:");
            inOrderStack(root);
            System.out.println();
        }

    }
