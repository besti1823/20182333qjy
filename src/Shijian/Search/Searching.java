package Shijian.Chazhao;

public class Searching<T>
{
    private T[] data;

    public void setData(T[] data) {
        this.data = data;
    }
   //课本上的代码
    public static Comparable linearSearch (Comparable[] data,
                                           Comparable target)
    {
        Comparable result = null;
        int index = 0;

        while (result == null && index < data.length)
        {
            if (data[index].compareTo(target) == 0)
                result = data[index];
            index++;
        }

        return result;
    }

    public static <T extends Comparable<T>>
    boolean binarySearch (Comparable[] data, int low, int high,int mid,Comparable target)
    {
        boolean found = false;

        if(data[mid].compareTo(target) == 0)
            found = true;
        else if (data[mid].compareTo(target) > 0)
        {
            if(low < mid-1)
            {
                mid--;
                found = binarySearch(data,low,high,mid,target);
            }
        }
        else if(mid+1 <= high)
        {
            mid++;
            found = binarySearch(data,low,high,mid,target);
        }
        return found;
    }

    public static <T extends Comparable<T>>
    Comparable binaryShow (Comparable[] data, int low, int high, int mid , Comparable target)
    {
        Comparable found = null;


        if(data[mid].compareTo(target) == 0)
            found = data[mid];
        else if (data[mid].compareTo(target) > 0)
        {
            if(low < mid-1)
            {
                mid--;
                found = binaryShow(data,low,high,mid,target);
            }
        }
        else if(mid+1 <= high)
        {
            mid++;
            found = binaryShow(data,low,high,mid,target);
        }
        return found;
    }
}