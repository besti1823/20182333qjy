package Shijian.Chazhao;

import java.util.Scanner;

public class SearchingTest {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("你想查找的数是（100以内）:");
        int c = scan.nextInt();
        Searching searching = new Searching();
        Comparable[] data = new Comparable[100];
        for(int i=0;i<100;i++)
            data[i] = i;
        searching.setData(data);
        boolean found = searching.binarySearch(data,1,99,50,c);
        System.out.println("你想查找的数是:"+ c );
        System.out.println("是否找到:" + found);
        Comparable a = searching.binaryShow(data,1,99,50,c);
        System.out.println("你想找的数在列表的第" + a +"位");
    }
}