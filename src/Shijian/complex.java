package Shijian;

import java.awt.*;
import java.util.*;

public class complex {
    private double RealPart;
    private double ImagePart;//定义实部和虚部

    public complex(double R, double I)//构造函数，接受成员值的传递，并进行保存
    {
        RealPart = R;
        ImagePart = I;
    }

    public double getRealPart()//通过get来进行数据封装
    {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public complex add(complex a)//创一个新的Complex用来保存相加后得到的复数
    {
        double I = ImagePart + a.getImagePart();//根据已有复数创建对象，复制复数a两个成员值。
        double R = RealPart + a.getRealPart();
        return new complex(R, I);//传递第一个值，改变成员值。
    }

    public complex sub(complex a) {
        double I = ImagePart - a.getImagePart();
        double R = RealPart - a.getRealPart();
        return new complex(R, I);
    }

    public complex mul(complex a) {
        double c = a.getRealPart();
        double d = a.getImagePart();
        double I = ImagePart * c - RealPart * d;
        double R = RealPart * c - ImagePart * d;
        return new complex(R, I);
    }

    public complex div(complex e) {
        double c = e.getRealPart();
        double d = e.getImagePart();
        double t = c * c + d * d;
        double R = (RealPart * c + ImagePart * d) / t;
        double I = (ImagePart * c - RealPart * d) / t;
        return new complex(R, I);
    }


    public boolean Compare(complex a) {
        double c = a.getRealPart();
        double d = a.getImagePart();
        double m1 = Math.sqrt(RealPart * RealPart + ImagePart * ImagePart);
        double m2 = Math.sqrt(c * c + d * d);
        if (m1 > m2)
            return false;
        else
            return true;
    }

    public boolean Equal(complex a) {
        if (this.equals(a))
            return true;
        else
            return false;
    }

    public String toString() {
        String S = "";
        if (ImagePart > 0)
            S = RealPart + "+" + ImagePart + "i";
        else if (ImagePart < 0)
            S = RealPart + "" + ImagePart + "i";
        else
            S = RealPart + "";
        return S;
    }


}