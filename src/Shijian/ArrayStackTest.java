package Shijian;

public class ArrayStackTest {
    public static void main(String[] args) throws EmptyCollectionException {
        ArrayStack stack=new ArrayStack();
        stack.push("Qian");
        stack.push("Jia");
        stack.push("Yu");
        System.out.println("输出stack："+stack);
        System.out.println("输出栈元素的个数："+stack.size());
        System.out.println("输出栈顶："+stack.peek());
        System.out.println("输出栈顶并删除："+stack.pop()+"\n");
        System.out.println("输出stack："+stack);
        System.out.println("输出栈元素的个数："+stack.size());
        System.out.println("输出栈顶："+stack.peek());
    }
}
