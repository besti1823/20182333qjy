package ASL;

public class Tree {
    private int i=-1;
    public Tree next=null;
    public Tree secondnext=null;
    public Tree(int i)
    {
        this.i=i;
    }
    public  int compareto(Tree a)
    {
        int result = i-a.geti();
        return result;
    }
    public int geti()
    {
        return  i;
    }
    public void setI(int i)
    {
        this.i=i;
    }

    @Override
    public String toString()
    {
        String string = i+"";
        return string;
    }
    public void setNext(Tree a)
    {
        next = a;
    }
    public void setSecondnext(Tree b)
    {
        secondnext = b;
    }
    public Tree getNext()
    {
        return  next;
    }
    public Tree getSecondnext()
    {
        return  secondnext;
    }
}
