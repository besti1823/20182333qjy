package ASL;
import java.util.Scanner;
public class LinearSearch {
    public static void main(String[] args) {
        int[] a = {19, 14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};
        System.out.println("原始数组数据如下：");
        for (int n : a) {
            System.out.print(n+" ");
        }
        System.out.println();
        System.out.println("哈希线性探测测试：");
        Linear result = new Linear(a);
        Scanner input=new Scanner(System.in);
        System.out.println("请输入你要查找的数：");
        int num=input.nextInt();
        System.out.print("你要查找的数是否存在：");
        System.out.println(result.search(num));
        System.out.println("线性列表如下:(第一行表示数所在线性数组的位置，第二行表示数据)");
        System.out.println(result);



    }
}
