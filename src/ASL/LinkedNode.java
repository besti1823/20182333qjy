package ASL;

public class LinkedNode {
    protected LinkedNode next;
    private int  element;

    public LinkedNode(int element) {
        this.element = element;
        this.next=null;
    }

    public LinkedNode(){
        this.next=null;
    }

    public void setNext(LinkedNode next) {
        this.next = next;
    }

    public void setElement(int element) {
        this.element = element;
    }

    public LinkedNode getNext() {
        return next;
    }

    public int getElement() {
        return element;
    }

}
