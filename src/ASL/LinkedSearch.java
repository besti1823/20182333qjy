package ASL;

import java.util.Scanner;

public class LinkedSearch {
    public static void main(String[] args) {
        int[] a={19,14,23,1,68,20,84,27,55,11,10,79};
        System.out.println("原始数组数据如下：");
        for (int n : a) {
            System.out.print(n+" ");
        }
        System.out.println();
        System.out.println("链式查找：");
        Linked[] linked=new Linked[16];
        LinkedNode temp =null;
        int m;
        for(int i=0;i<a.length;i++){
            m=a[i]%11;
            linked[m]=new Linked();
            if(linked[m].getnQJY()==0){
                linked[m].setHead(new LinkedNode(a[i]));
            }
            else{
                temp=linked[m].getHead();
                while(temp!=null){
                    temp=temp.next;
                }
                temp.setElement(a[i]);
            }
        }

        System.out.println("请输入需要查找的元素：");
        Scanner in=new Scanner(System.in);
        int goal=in.nextInt();
        int m1=goal%11;
        int count=0;
        temp=linked[m1].getHead();
        while(temp.getElement()!=goal){
            temp=temp.next;
            count++;
        }
        System.out.println("该元素在第"+m1+"行"+"第"+(count+1)+"位");
    }


}
