package ASL;

public class Linked {
    private LinkedNode head;
    private LinkedNode temp;
    private int count=0;

    public int getCount() {
        return count;
    }

    public Linked() {
        head=null;
    }
    public LinkedNode getHead(){
        return head;
    }
    //在末尾添加元素
    public void Add(LinkedNode a){
        if(head==null){
            head=a;
        }
        else {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = a;
        }
    }
    //在结点删去ele元素
    public void SubJ(int ele){
        temp=head;
        while(temp.next.getElement()!=ele){
            temp=temp.next;
        }
        temp.next=temp.next.next;
    }
    //在结点添加元素
    public void AddJ(int ele,int ele1){
        temp=head;
        while(temp.getElement()!=ele){
            temp=temp.next;
        }
        LinkedNode a=new LinkedNode(ele1);
        a.next=temp.next;
        temp.next=a;
    }

    //在开头增加元素
    public void AddH(int ele1){
        LinkedNode a=new LinkedNode(ele1);
        a.next=head;
        head=a;
    }

    public String toString(){
        LinkedNode temp1=head;
        String result="";
        while(temp1.next!=null){
            result+=temp1.getElement()+"\t";
            temp1=temp1.next;
        }
        result+=temp.getElement();
        return result;
    }
    //第一个计数
    public int getnQJY(){
        if(head==null)
            return 0;
        int nQJY=1;
        LinkedNode temp1=head;
        while(temp1.next!=null){
            nQJY++;
            temp1=temp1.next;
        }
        return nQJY;
    }

    //第二个计数


    public void setHead( LinkedNode head) {
        this.head = head;
    }

    public void Sort(){
        temp=head;
        int k;
        LinkedNode temp1=null;
        while(temp.next!=null){
            temp1=temp.next;
            while(temp1.next!=null){
                if(temp1.getElement()>temp.getElement()){
                    k=temp1.getElement();
                    temp1.setElement(temp.getElement());
                    temp.setElement(k);
                }
                temp1=temp1.next;
            }
            System.out.println("当前链表元素个数为："+this.getnQJY()+"元素："+this.toString());
            temp=temp.next;
        }
    }

    public LinkedNode Count(int index){
        temp=head;
        for(int i=1;i<index;i++){
            temp=temp.next;
        }
        return temp;
    }
}
