package ASL;
import java.util.*;
public class ShunXuSearch {
    public static void main(String[] arg) {
        int[] a={19,14, 23, 1, 68, 20, 84, 27, 55, 11, 10, 79};
        System.out.println("数组数据如下：");
        for (int n : a) {
            System.out.print(n+" ");
        }
        System.out.println();
        Scanner input=new Scanner(System.in);
        System.out.println("请输入你要查找的数：");
        //存放控制台输入的语句
        int num=input.nextInt();
        //调用searc()方法，将返回值保存在result中
        int result=search(a, num);
        if(result==-1){
            System.out.println("你输入的数不存在与数组中。");
        }
        else
            System.out.println("你输入的数字存在，在数组中的位置是第："+(result+1)+"个");
    }
    public static int search(int[] a, int num) {
        for(int i = 0; i < a.length; i++) {
            if(a[i] == num){//如果数据存在
                return i;//返回数据所在的下标，也就是位置
            }
        }
        return -1;//不存在的话返回-1
    }
}
