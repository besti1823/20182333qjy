package ASL;

public class Linear {
    private int addr;
    private int count=0;
    final int MAXSIZE=11;
    protected int[] ele=new int[MAXSIZE];
    final int Initial=-12;
    public Linear(){
        for(int i=0;i<MAXSIZE;i++){
            ele[i]=Initial;
        }
    }
    public Linear(int[] key){
        for(int i=0;i<MAXSIZE;i++){
            ele[i]=Initial;
        }
        for(int j=0;j<ele.length;j++){
            Insert(key[j]);
        }
    }
    public void Insert(int key){
        int addr=hash(key);
        while(ele[addr]!=Initial){
            addr=hash(addr+1);
        }
        ele[addr]=key;
        count++;
    }
    public int hash(int key){
        return key%MAXSIZE;
    }
    public boolean search(int elem){
        int addr = hash(elem);
        while(this.ele[addr] != elem) {
            addr = hash(addr + 1);
            if(this.ele[addr] == Initial || addr == hash(elem)) {
                return false;
            }
        }
        return true;
    }
    public String toString(){
        String result="";
        String result2="";
        for(int i=0;i<MAXSIZE;i++){
            result+=(ele[i]+" ");
            result2+=(i+"  ");
        }
        return (result2+"\n"+result);
    }
}
