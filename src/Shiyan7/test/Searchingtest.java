package Shiyan7.test;

import Shiyan7.cn.edu.besti.cs1823.Q2333.Searching;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SearchingTest {
    Integer[] A ={1,2,3,4,2018,2333};
    Integer[] B ={2,3,3,3};
    Integer[] C ={20,18,23,33};
    Integer[] D ={1,3,1,4};
    Integer[] E ={1,2,3,4};
    Integer[] F ={20,18,23,33};
    Integer[] G ={20,18,2333};
    Integer[] H ={23,33};
    Integer[] I ={2,3,4,5,6};
    Integer[] J ={3,6,9,12,15};

    @Test
    public void testlinearSearch() {
        assertEquals(true, Searching.linearSearch(A,0,A.length-1,2333) );//正常情况
    }
    @Test
    public void testlinearSearch1() {
        assertEquals(true, Searching.linearSearch(B,0,B.length+1,2) );//正常情况
    }

    @Test
    public void testlinearSearch2() {
        assertEquals(true, Searching.linearSearch(C,0,C.length+1,33) );//正常情况
    }
    @Test
    public void testlinearSearch3() {
        assertEquals(false,Searching.linearSearch(D,0,D.length-1,2333) );//异常情况
    }
    @Test
    public void testlinearSearch4() {
        assertEquals(false,Searching.linearSearch(E,0,E.length-1,2018) );//异常情况
    }
    @Test
    public void testlinearSearch5() {
        assertEquals(false,Searching.linearSearch(F,0,F.length-1,20182333) );//异常情况
    }
    @Test
    public void testlinearSearch6() {
        assertEquals(false,Searching.linearSearch(G,3,G.length-1,33) );//边界情况
    }

    @Test
    public void testlinearSearch7() {
        assertEquals(false,Searching.linearSearch(H,2,H.length-1,2) );//边界情况
    }

    @Test
    public void testlinearSearch8() {
        assertEquals(true,Searching.linearSearch(I,0,I.length+1,2) );//边界情况
    }

    @Test
    public void testlinearSearch9() {
        assertEquals(true,Searching.linearSearch(J,0,J.length+1,3) );//边界情况
    }



}