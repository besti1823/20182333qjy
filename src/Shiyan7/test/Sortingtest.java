//package Shiyan7.test;
//
//import Shiyan7.LinkedBinarySearchTree;
//import Shiyan7.cn.edu.besti.cs1823.Q2333.Sorting;
//import org.junit.Test;
//
//public class Sortingtest {
//    @Test
//    public void ShellSort() {
//        Comparable num[]={3, 2, 1, 20, 18, 23, 33};
//        System.out.println("希尔排序:");
//        Sorting.ShellSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//
//    @Test
//    public void HeapSort() {
//        int num[]={20,18,23,33,9,18,1};
//        System.out.println("堆排序:");
//        Sorting.HeapSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//
//    @Test
//    public void selectionSort() {
//        Comparable num[]={1, 23, 20, 14, 99};
//        System.out.println("选择排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//
//
//    @Test
//    public void bubbleSort() {
//        Comparable num[]={46,66,24,9,29,30};
//        System.out.println("冒泡排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//    @Test
//    public void mergeSort() {
//        Comparable num[]={33,23,20,18,66,89,5,45,26};
//        System.out.println("归并排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//    @Test
//    public void LinkedBinarySearchTree() {
//        Comparable num[] = {8,18,1,6,23,33,20,18,12};
//        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
//        tree.addElement(8);
//        tree.addElement(18);
//        tree.addElement(1);
//        tree.addElement(6);
//        tree.addElement(23);
//        tree.addElement(33);
//        tree.addElement(20);
//        tree.addElement(18);
//        tree.addElement(12);
//        System.out.println("\n二叉树排序:");
//        for (int i =0;i<num.length;i++){
//            System.out.print(tree.findMin() + " ");
//            tree.removeElement(tree.findMin());
//        }
//        System.out.println();
//    }
//    @Test
//    public void insertionSort() {
//        Comparable num[]={2,78,23,14,5,36,16,52};
//        System.out.println("插入排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//    @Test
//    public void quickSort() {
//        Comparable num[]={18,3,45,25,98,36,47};
//        System.out.println("快速排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//    @Test
//    public void IntervalSort() {
//        Comparable num[]={99,36,2,8,19,48,21};
//        System.out.println("间隔排序:");
//        Sorting.selectionSort(num);
//        for (int i =0;i<num.length;i++){
//            System.out.println(num[i]+" ");
//        }
//    }
//
//}
