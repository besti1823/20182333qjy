package Shiyan7.cn.edu.besti.cs1823.Q2333;

public class Searching {
    public static<T extends Comparable<? super T>> boolean linearSearch(T[] data, int min, int max,T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max){
            if (data[index].compareTo(target)==0)
                found = true;
            index++;
        }
        return found;
    }
    private static <T extends Comparable<? super T>> void swap(T[] data, int i, int j) {
        T temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

    //线性查找
    public static Comparable linearSearch(Comparable[] list, Comparable target)
    {
        int index = 0;
        boolean found = false;

        while (!found && index < list.length)
        {
            if (list[index].equals(target))
                found = true;
            else
                index++;
        }

        if (found)
            return list[index];
        else
            return null;
    }


    //顺序查找
    public static int SequenceSearch(int a[], int value, int n) {
        int i;
        for (i = 0; i < n; i++) {
            if (a[i] == value)
                return i;

        }
        return -1;
    }

    //二分查找（折半查找）
    public static int BinarySearch1(int a[], int value, int n) {
        int low, high, mid;
        low = 0;
        high = n - 1;
        while (low <= high) {
            mid = (low + high) / 2;
            if (a[mid] == value)
                return mid;
            if (a[mid] > value)
                high = mid - 1;
            if (a[mid] < value)
                low = mid + 1;
        }
        return -1;
    }

    //二分查找，递归版本
    public static int BinarySearch2(int a[], int value, int low, int high) {
        int mid = low + (high - low) / 2;
        if (a[mid] == value)
            return mid;
        if (a[mid] > value)
            return BinarySearch2(a, value, low, mid - 1);
        if (a[mid] < value)
            return BinarySearch2(a, value, mid + 1, high);
        return -1;
    }

    //插值查找
    public static int InsertionSearch(int a[], int value, int low, int high) {
        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (low > high)
            return -1;
        if (a[mid] == value)
            return mid;
        if (a[mid] > value)
            return InsertionSearch(a, value, low, mid - 1);
        else
            return InsertionSearch(a, value, mid + 1, high);

    }


    /*定义斐波那契查找法*/
    public static int FibonacciSearch(int[] a, int n, int key)  //a为要查找的数组,n为要查找的数组长度,key为要查找的关键字
    {
        int low = 0;
        int high = n - 1;
        //定义一个斐波那契数组
        int max = 20;
        int[] F = new int[max];
        F[0] = 1;
        F[1] = 1;
        for (int i = 2; i < max; i++) {
            F[i] = F[i - 1] + F[i - 2];
        }

        int k = 0;
        while (n > F[k] - 1)//计算n位于斐波那契数列的位置
            k++;

        int[] temp;//将数组a扩展到F[k]-1的长度
        temp = new int[F[k] - 1];
        for (int x = 0; x < a.length; x++) {
            temp[x] = a[x];
        }

        for (int i = n; i < F[k] - 1; ++i)
            temp[i] = a[n - 1];

        while (low <= high) {
            int mid = low + F[k - 1] - 1;
            if (key < temp[mid]) {
                high = mid - 1;
                k -= 1;
            } else if (key > temp[mid]) {
                low = mid + 1;
                k -= 2;
            } else {
                if (mid < n)
                    return mid; //若相等则说明mid即为查找到的位置
                else
                    return n - 1; //若mid>=n则说明是扩展的数值,返回n-1
            }
        }
        return -1;
    }





    //哈希查找
    public static void hashinsert(int[] hashTable, int target) {

        int hashAddress = hash(hashTable, target);
        while (hashTable[hashAddress] != 0) {
            hashAddress = (++hashAddress) % hashTable.length;
        }
        hashTable[hashAddress] = target;
    }

    public static int hashsearch(int[] hashTable, int target) {
        int hashAddress = hash(hashTable, target);
        while (hashTable[hashAddress] != target) {
            hashAddress = (++hashAddress) % hashTable.length;
            if (hashTable[hashAddress] == 0 || hashAddress == hash(hashTable, target)) {
                return -1;
            }
        }
        return hashAddress;
    }

    public static int hash(int[] hashTable, int target) {
        return target % hashTable.length;
    }

    //分块查找
    public static boolean BlockSearch(Comparable[] data ,Comparable target,int lump){
        if(target == blockSearch(data,target,lump)) {
            return true;
        } else {
            return false;
        }
    }
    private static Comparable blockSearch(Comparable[] data ,Comparable target,int lump){
        int temp = data.length / lump;
        Comparable result = null;
        Comparable[] tp= new Comparable [temp];
        for (int a = 0;a< data.length; a = a+ temp){
            if (data[a].compareTo(target) > 0){
                int c = 0;
                for (int j =a - temp; j < a; j++){
                    tp[c] = data[j];
                    c++;
                }
                result = linearSearch(tp,target);
                break;
            }
            else if (data[a].compareTo(target) == 0){
                result = data[a];
                break;
            }
        }
        return result;
    }
}
