import java.util.*;
import java.text.*;
public class pp3_5
{
    public static void main(String[]args)
    {
        int r;
        double s,v;
        Scanner scan=new Scanner(System.in);
        System.out.print("请输入球的半径：");
        r=scan.nextInt();
        s=4/3*Math.PI*Math.pow(r,3);
        v=4*Math.PI*Math.pow(r,2);
        DecimalFormat fmt = new DecimalFormat ("0.####");
        System.out.println("球的面积是：" + fmt.format(s));
        System.out.println("球的体积是：" + fmt.format(v));
        
    }
}
